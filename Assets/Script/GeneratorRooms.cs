using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using static UnityEditor.PlayerSettings;

public class GeneratorRooms : MonoBehaviour
{
    // Room
    public GameObject[] availableRooms;
    public List<GameObject> currentRooms;
    private float screenWidthInPoints;

    // Ennemies
    public GameObject[] availableObjects;
    public List<GameObject> objects;

    public List<GameObject> availableCoins;
    public List<GameObject> currentCoins;

    public float objectsMinDistance = 5.0f;
    public float objectsMaxDistance = 10.0f;

    public float objectsMinY = -14f;
    public float objectsMaxY = 14f;

    public float objectsMinRotation = -45.0f;
    public float objectsMaxRotation = 45.0f;

    private Rigidbody2D enemyRigidbody;

    public List<GameObject> getListObject()
    {
        return this.objects;
    }
    public GameObject[] getListObjectAvailble()
    {
        return this.availableObjects;
    }

    void Start()
    {
        float height = 2.0f * Camera.main.orthographicSize;
        screenWidthInPoints = height * Camera.main.aspect;
        StartCoroutine(GeneratorCheck());
    }

    // Update is called once per frame
    void Update()
    {

    }
    void AddRoom(float farthestRoomEndX)
    {
        //1
        int randomRoomIndex = Random.Range(0, availableRooms.Length);
        //2
        GameObject room = (GameObject)Instantiate(availableRooms[randomRoomIndex]);
        //3
        float roomWidth = room.transform.Find("Floor").localScale.x;
        //4
        float roomCenter = farthestRoomEndX + roomWidth * 0.5f;
        //5
        room.transform.position = new Vector3(roomCenter, room.transform.position.y, 0);
        //6
        currentRooms.Add(room);
    }

    private void GenerateRoomIfRequired()
    {
        //1
        List<GameObject> roomsToRemove = new List<GameObject>();
        //2
        bool addRooms = true;
        //3
        float playerX = transform.position.x;
        //4
        float removeRoomX = playerX - screenWidthInPoints;
        //5
        float addRoomX = playerX + screenWidthInPoints;
        //6
        float farthestRoomEndX = 0;
        foreach (var room in currentRooms)
        {
            //7
            float roomWidth = room.transform.Find("Floor").localScale.x;
            float roomStartX = room.transform.position.x - (roomWidth * 0.5f);
            float roomEndX = roomStartX + roomWidth;
            //8
            if (roomStartX > addRoomX)
            {
                addRooms = false;
            }
            //9
            if (roomEndX < removeRoomX)
            {
                roomsToRemove.Add(room);
            }
            //10
            farthestRoomEndX = Mathf.Max(farthestRoomEndX, roomEndX);
        }
        //11
        foreach (var room in roomsToRemove)
        {
            currentRooms.Remove(room);
            Destroy(room);
        }
        //12
        if (addRooms)
        {
            AddRoom(farthestRoomEndX);
        }
    }
    void AddObject(float lastObjectX)
    {
        //1
        int randomIndex = Random.Range(0, availableObjects.Length);
        //2
        GameObject obj = (GameObject)Instantiate(availableObjects[randomIndex]);
        //3
        //float objectPositionX = lastObjectX + Random.Range(0, objectsMaxDistance);

        float objectPositionX = lastObjectX+ 45.0f;

        float randomY = Random.Range(objectsMinY, objectsMaxY);
        if (randomIndex == 0)
        {
            obj.transform.position = new Vector3(objectPositionX, randomY, 0);
        }
        if (randomIndex==1)
        {
            obj.transform.position = new Vector3(objectPositionX, -7, 0);
        }
        //4
        //float rotation = Random.Range(objectsMinRotation, objectsMaxRotation);
        //obj.transform.rotation = Quaternion.Euler(Vector3.forward * rotation);
        ////5
        objects.Add(obj);
    }
    void GenerateObjectsIfRequired()
    {
        //1
        float playerX = transform.position.x;
        float removeObjectsX = playerX - screenWidthInPoints;
        float addObjectX = playerX + screenWidthInPoints;
        float farthestObjectX = 0;
        //2
        List<GameObject> objectsToRemove = new List<GameObject>();
        foreach (var obj in objects)
        {
            //3
            float objX = obj.transform.position.x;
            //4
            farthestObjectX = Mathf.Max(farthestObjectX, objX);
            //farthestObjectX = 20.0f;

            int randomGenCoin = Random.RandomRange(0, 3);
            //Debug.Log("coin" + randomGenCoin);

            //5
            if (objX < removeObjectsX)
            {
                Debug.Log("coin" + obj);
                if (randomGenCoin == 0)
                {
                    Vector3 pos = obj.transform.position;
                    GameObject coin = (GameObject)Instantiate(availableCoins[0]);
                    coin.transform.position = pos;
                    
                    currentCoins.Add(coin);
                }
                objectsToRemove.Add(obj);
            }
        }
        foreach (var obj in objects)
        {
            bool getRigidbody = obj.GetComponent<Rigidbody2D>();
            if (getRigidbody)
            {
                Rigidbody2D objRigidbody = obj.GetComponent<Rigidbody2D>();
                Vector2 newVelocity = objRigidbody.velocity;
                newVelocity.x = -3;
                objRigidbody.velocity = newVelocity;
            }
            //Vector3 posObject = obj.transform.position;
            //posObject.x -=3;
            //obj.transform.position = posObject;
        }
        //6

       

        foreach (var obj in objectsToRemove)
        {
            objects.Remove(obj);
            Destroy(obj);

        }
        //7
        if (farthestObjectX < addObjectX)
        {
            AddObject(farthestObjectX);
        }
    }
    private IEnumerator GeneratorCheck()
    {
        while (true)
        {
            GenerateRoomIfRequired();
            GenerateObjectsIfRequired();  
            yield return new WaitForSeconds(0.25f);
        }
    }

    


}
