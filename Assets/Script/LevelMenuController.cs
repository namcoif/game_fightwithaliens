using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMenuController : MonoBehaviour
{
    public void playGame1()
    {
        Application.LoadLevel("Level1");
    }

    public void playGame2()
    {
        Application.LoadLevel("Level2");
    }

    public void playGame3()
    {
        Application.LoadLevel("Level3");
    }
    public void BackToMenu()
    {
        Application.LoadLevel("MainMenu");
    }
}
