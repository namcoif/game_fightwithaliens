using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TextCore.Text;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    public float jetpackForce = 75.0f;
    float forwardMovementSpeed = 20.0f;

    private bool isDead = false;

    public AudioClip gunSound;


    private Rigidbody2D playerRigidbody;

    private GeneratorRooms generatorRooms;
    public Animator characterAnimator;

    List<GameObject> listEnemies;
    GameObject[] listEnemiesAvailable;

    GameObject firtsEnemy;
    Animator animatorEnemy;
    Vector2 newVelocity;
    Vector2 velocityBullet;

    int countHit = 0;
    int indexEnemy = 0;
    int kill = 0;
    int index = 0;
    bool onTarget = false;

    public Image healthBar;
    float health = 1000;
    float healthMax = 1000;
    float dameEnemyA = 1;
    float dameEnemyB = 3;
    float dameEnemyC = 5;


    int dameEnemyABeHit = 40;
    int dameEnemyBBeHit = 100;
    int dameEnemyCBeHit = 1500;

    float bulX;
    float bulY;

    bool isEnemyA = false;
    bool isEnemyB = false;
    bool isEnemyC = false;

    string nameEnemyA;
    string nameEnemyB;
    string nameEnemyC;


    int totalEnemyA = 0;
    int totalEnemyB = 0;
    int totalEnemyC = 0;

    public bool isGameWin = false;
    bool isGameOver = false;

    public Text maxEnemyA;
    public Text countCurrentEnemyA;

    public Text maxEnemyB;
    public Text countCurrentEnemyB;

    public GameObject[] availableBullets;
    public List<GameObject> currentBullets = new List<GameObject>();
    List<GameObject> bulletsToRemove = new List<GameObject>();

    string sceneName;
    public void setTotalEnemyC()
    {
        this.totalEnemyC = 1;
    }
    public int getTotalEnemyC()
    {
       return this.totalEnemyC;
    }
    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        newVelocity = playerRigidbody.velocity;
        generatorRooms = GetComponent<GeneratorRooms>();

        listEnemies=generatorRooms.getListObject();
        listEnemiesAvailable = generatorRooms.getListObjectAvailble();

        if (sceneName=="Level3")
        {
            health = 8000;
        }
        firtsEnemy = listEnemies.FirstOrDefault();
        sceneName = SceneManager.GetActiveScene().name;
        if (listEnemiesAvailable.Length>0)
        {
             nameEnemyA= listEnemiesAvailable[0].name + "(Clone)";

        }
        if (listEnemiesAvailable.Length > 1)
        {
            nameEnemyB = listEnemiesAvailable[1].name + "(Clone)";
        }

        switch (sceneName)
        {
            case "Level1":
                maxEnemyA.text += "15";
                break;
            case "Level2":
                maxEnemyA.text += "10";
                if (maxEnemyB!=null)
                {
                    maxEnemyB.text = "/5";
                }
                break;
            case "Level3":
                if (maxEnemyB != null)
                {
                    maxEnemyB.text = "/10";
                }
                maxEnemyA.text += "15";
                break;
            default:
                break;
        }
       
    }

    void Update()
    {
        Debug.Log("Total C: " + totalEnemyC);
        endGame();

    }
    private void FixedUpdate()
    {
        
        characterAction();
        checkStatusGame();
        Debug.Log("Total A: " + totalEnemyA);
        Debug.Log("Game win: " + isGameWin);
        StartCoroutine(GeneratorCheck());
        countCurrentEnemyA.text = totalEnemyA.ToString();
        if (countCurrentEnemyB!=null)
        {
            countCurrentEnemyB.text=totalEnemyB.ToString();
        }

    }
    private void characterAction()
    {
        List<bool> button=new List<bool>();
        float shoot_1 = Input.GetAxisRaw("Fire1");
        float walk_1 = Input.GetAxisRaw("Horizontal");
        float jump_1 = Input.GetAxisRaw("Vertical");
        firtsEnemy = listEnemies.FirstOrDefault();

        if (walk_1>0)
        {

            characterAnimator.SetTrigger("Walk_1");
            

            newVelocity.x = forwardMovementSpeed;
            newVelocity.y = 0;
            playerRigidbody.velocity = newVelocity;


        }
        else if (walk_1<0)
        {
            characterAnimator.SetTrigger("Walk_1");
            

            newVelocity.x = -forwardMovementSpeed;
            newVelocity.y = 0;

            playerRigidbody.velocity = newVelocity;
        }
        else if (jump_1>0)
        {
            characterAnimator.SetTrigger("Jump_2");

        }

        else if(jump_1<=0)
        {
            newVelocity.x = 0;
            playerRigidbody.velocity = newVelocity;
            if (shoot_1>0)
            {
                animatorEnemy = firtsEnemy.GetComponent<Animator>();
                AudioSource.PlayClipAtPoint(gunSound, transform.position);

                characterAnimator.SetTrigger("Shoot_1");
                animatorEnemy.SetTrigger("beHit");

                if ((transform.position.x + 80f) > listEnemies[0].transform.position.x)
                {
                    countHit++;
                }

                

                if (firtsEnemy.name == nameEnemyA)
                {
                    if (countHit >= dameEnemyABeHit)
                    {
                        animatorEnemy.ResetTrigger("beHit");
                        animatorEnemy.SetTrigger("dead");
                        if (listEnemies.Count > 1)
                        {

                            Destroy(listEnemies[0]);
                            listEnemies.RemoveAt(0);
                            totalEnemyA++;
                        }

                        countHit = 0;

                    }
                }
                if (firtsEnemy.name == nameEnemyB)
                {
                    if (countHit >= dameEnemyBBeHit)
                    {
                        animatorEnemy.ResetTrigger("beHit");
                        animatorEnemy.SetTrigger("dead");
                        if (listEnemies.Count > 1)
                        {

                            Destroy(listEnemies[0]);
                            listEnemies.RemoveAt(0);
                            totalEnemyB++;
                        }

                        countHit = 0;

                    }
                }
            }
            else
            {
                firtsEnemy = generatorRooms.getListObject().FirstOrDefault();

                animatorEnemy = firtsEnemy.GetComponent<Animator>();

                characterAnimator.SetTrigger("Reset");
                animatorEnemy.ResetTrigger("beHit");


            }


        }
       

    }
    void genBullet()
    {
        float enemyX = firtsEnemy.transform.position.x;
        float enemyY = firtsEnemy.transform.position.y;
        GameObject bullet=null;


        if (firtsEnemy.name == listEnemiesAvailable[0].name+"(Clone)") {
            bullet = (GameObject)Instantiate(availableBullets[0]);
        }
        if (listEnemiesAvailable.Length > 1)
        {
            if (firtsEnemy.name == listEnemiesAvailable[1].name + "(Clone)")
            {
                bullet = (GameObject)Instantiate(availableBullets[1]);
            }
        }
        


        bulX = enemyX-8f;
        bulY= enemyY-2f;

            bullet.transform.position= new Vector3(bulX, bulY, 0);
            Rigidbody2D rigidbody2D = bullet.GetComponent<Rigidbody2D>();

        bulX = Random.Range(-40f, -20f);
        bulY = Random.Range(-10f, 10f);

        velocityBullet = new Vector2(bulX,bulY);
        rigidbody2D.velocity = velocityBullet;
        Collider2D bulletColider= bullet.GetComponent<Collider2D>();
        bulletColider.enabled=true;

        currentBullets.Add(bullet);
       
    }
    void GenerateBulletsIfRequired()
    {
        firtsEnemy = listEnemies[0];

        float enemyX = firtsEnemy.transform.position.x;

        foreach(var bull in currentBullets)
        {
            if (firtsEnemy.transform.position.x - bull.transform.position.x > 75f || onTarget==true)
            {
                bulletsToRemove.Add(bull);
                onTarget= false;
            }
        }
        foreach (var bull in bulletsToRemove)
        {
            currentBullets.Remove(bull);
            Destroy(bull);

        }
        if (currentBullets.Count<5)
        {
            genBullet();
        }
    }
    private IEnumerator GeneratorCheck()
    {
        while (true)
        {
            GenerateBulletsIfRequired();
            yield return new WaitForSeconds(500f);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        int length = listEnemiesAvailable.Length;
        if (firtsEnemy.name == listEnemiesAvailable[0].name + "(Clone)")
        {
            minusHP(dameEnemyA);
        }
        if (length > 1)
        {
            if (firtsEnemy.name == listEnemiesAvailable[1].name + "(Clone)")
            {
                minusHP(dameEnemyB);
            }
        }
        if (length > 2)
        {
            if (firtsEnemy.name == listEnemiesAvailable[2].name + "(Clone)")
            {
                minusHP(dameEnemyC);
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        minusHP(dameEnemyC);

    }

    public void minusHP(float _damage)
    {
        health -= _damage;
        healthBar.fillAmount = health / healthMax;
    }
    public void checkStatusGame()
    {
        if (health <= 0) {
            isGameOver = true;
        }

        switch (sceneName)
        {
            case "Level1":
                if (totalEnemyA >= 15)
                {
                    isGameWin= true;
                }
                break;
            case "Level2":
                if (totalEnemyA >= 10 && totalEnemyB>=5)
                {
                    isGameWin = true;
                }
                break;

            case "Level3":
                if (totalEnemyA >= 15 && totalEnemyB >= 10 && totalEnemyC==1)
                {
                    isGameWin = true;
                }
                break;

            default:
                break;
        }
    }
    public void endGame()
    {
        if (isGameWin == true)
        {
            Application.LoadLevel("win");
        }
        if (isGameOver == true)
        {
            Application.LoadLevel("gameOver");
        }
    }
}
