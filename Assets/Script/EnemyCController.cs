using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using static UnityEngine.GraphicsBuffer;

public class EnemyCController : MonoBehaviour
{

    CharacterController character;

    GameObject firtsEnemy;
    Vector2 velocityBullet;

    public Animator animatorEnemy;


    bool onTarget = false;

    float dameEnemyC = 5;

    int dameEnemyCBeHit = 100;

    int countHit;

    float bulX;
    float bulY;

    float enemyY;
    float enemyX;

    public GameObject[] availableBullets;
    public List<GameObject> currentBullets = new List<GameObject>();
    List<GameObject> bulletsToRemove = new List<GameObject>();

    string sceneName;
    void Start()
    {
        animatorEnemy = gameObject.GetComponent<Animator>();
        character=FindObjectOfType<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(GeneratorCheck());

    }

    private void FixedUpdate()
    {
        characterAction();
    }
    void genBullet()
    {
        GameObject bullet = null;
        bullet = (GameObject)Instantiate(availableBullets[0]);

        bulX = enemyX +25;
        bulY = enemyY ;

        bullet.transform.position = new Vector3(bulX, bulY, 0);
        Rigidbody2D rigidbody2D = bullet.GetComponent<Rigidbody2D>();

        bulX = Random.Range(-40f, -20f);
        bulY = 1;

        velocityBullet = new Vector2(bulX, bulY);
        rigidbody2D.velocity = velocityBullet;
        Collider2D bulletColider = bullet.GetComponent<Collider2D>();

        currentBullets.Add(bullet);

    }
    void GenerateBulletsIfRequired()
    {


        foreach (var bull in currentBullets)
        {
            if (transform.position.x - bull.transform.position.x > 75f /*|| bull.transform.position.y<1f*/ || onTarget == true || bull.GetComponent<Rigidbody2D>().velocity.x>=0)
            {
                bulletsToRemove.Add(bull);
                onTarget = false;
            }
        }
        foreach (var bull in bulletsToRemove)
        {
            currentBullets.Remove(bull);
            Destroy(bull);

        }
        if (currentBullets.Count < 5)
        {
            genBullet();
        }
    }
    private IEnumerator GeneratorCheck()
    {
        while (true)
        {
            GenerateBulletsIfRequired();
            yield return new WaitForSeconds(2);
        }
    }

    private void characterAction()
    {
        float shoot_1 = Input.GetAxisRaw("Fire1");

        if (shoot_1 > 0)
        {
            countHit++;
            animatorEnemy.SetTrigger("beHit");

        }
        else
        {
            animatorEnemy.ResetTrigger("beHit");
        }
        if (countHit>=1500)
        {
            character.setTotalEnemyC();
            animatorEnemy.SetTrigger("Explosion");
            Destroy(gameObject);
        }
    }
}