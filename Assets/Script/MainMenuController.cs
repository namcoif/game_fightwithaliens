using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    // Start is called before the first frame update
    public void playGame()
    {
        Application.LoadLevel("LevelMenu");
    }

    public void settingGame()
    {
        Application.LoadLevel("Setting");
    }

    public void inforGame()
    {
        Application.LoadLevel("Infor");
    }

    //public void exitGame()
    //{
    //    Application.LoadLevel("LevelMenu");
    //}

    public void BackToMenu()
    {
        Application.LoadLevel("MainMenu");
    }
}
